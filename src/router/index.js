import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/views/Login'
import Home from '@/views/Home'
import welcome from '@/views/welcome/welcome'
import User from '@/views/user/User'
import Rights from '@/views/right/Rights'
import Roles from '@/views/right/Roles'

Vue.use(Router)

const router =  new Router({
  mode:'history',
  routes: [
    
    {
      path:'/login',
      name:'Login',
      component:Login
    },
    {
      path:'/',
      name:'Home',
      component:Home,
      redirect:{path:'welcome'},
      children:[
        {
          path:'welcome',
          name:'welcome',
          component:welcome
        },{
          path:'Users',
          name:'users',
          component:User
        },{
          name:'Rights',
          path:'rights',
          component:Rights
        },{
          name:'Roles',
          path:'roles',
          component:Roles
        }
      ]
    }
  ]
})
//只要进行了路由跳转就会触发beforeEach 
//使用router-link 进行路由跳转
//编程式导航this.$router.push('/home')
//直接在地址栏中输入路由路径也可以进行路由跳转
//通过next()函数的调用进行跳转

//axios拦截器触发时机
//发送请求(get/post/delete/put)
//拦截了请求之后可以附加其它一些配置项，然后调用return config 才可以继续发送请求

//注册一个全局守卫，作用是在路由跳转前，对路由进行判断，防止未登录的用户跳转到其它需要登录的页面去
router.beforeEach((to, from, next) => {
  let token = localStorage.getItem('mytoken')
  //如果已经登录，那我不干涉你，让你随便访问
  if(token) {
    next()
  } else {
    if(to.path!=='/Login'){
      //如果没有登录，但你访问的login,那就不干涉你,让你访问
      next({path:'Login'})
    }else{
      next()
    }
  }
})

export default router