// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import elementUI from 'element-ui'
import store from '@/store/store'
//导入element-ui的css
import 'element-ui/lib/theme-chalk/index.css'

import '@/styles/index.scss'
//vue.use可以将elementUI中的所有组件全部注册成全局组件
Vue.use(elementUI)
Vue.config.productionTip = false



/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
